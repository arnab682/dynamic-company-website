<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Multipic;
use Image;
use Illuminate\Support\Carbon;

class MultipicController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }


    public function index(){
        $images = Multipic::all();
        return view('backend.multipic.index',compact('images'));
    }


    public function store(Request $request){

       $image =  $request->file('image');

       foreach($image as $multi_img){

       $name_gen = hexdec(uniqid()).'.'.$multi_img->getClientOriginalExtension();
       Image::make($multi_img)->resize(300,300)->save('image/multi/'.$name_gen);

       $last_img = 'image/multi/'.$name_gen;

       Multipic::insert([

           'image' => $last_img,
           'created_at' => Carbon::now()
       ]);
           } // end of the foreach



        return Redirect()->back()->with('success','Multi Image Inserted Successfully');


    }
}
