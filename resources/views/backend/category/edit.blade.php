@extends('backend.layouts.app')

@section('content')


    <div class="py-12">
        <div class="container">
            <div class="row">




                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header"> Edit Category </div>
                        <div class="card-body">



                        <form action="{{ URL::to('category/'.$categories->id)  }}" method="POST">
                            @csrf {{ method_field('put') }}
                            <div class="form-group">
                                <label for="exampleInputEmail1">Update Category Name</label>
                                <input type="text" name="category_name" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{ $categories->category_name }}">

                                    @error('category_name')
                                        <span class="text-danger"> {{ $message }}</span>
                                    @enderror

                            </div>

                            <button type="submit" class="btn btn-primary">Update Category</button>
                        </form>

                        </div>

                    </div>
                </div>



            </div>
        </div>

    </div>

@endsection
