
@include('backend.layouts.partials.header')
          <!--
      ====================================
      ——— LEFT SIDEBAR WITH FOOTER
      =====================================
    -->
    @include('backend.layouts.partials.sidebar')

      <!-- Header -->
      @include('backend.layouts.partials.navbar')

      {{-- main content --}}
      @yield('content')


@include('backend.layouts.partials.footer')
