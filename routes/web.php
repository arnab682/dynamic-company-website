<?php

use App\Http\Controllers\HelloController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\BrandController;
use App\Http\Controllers\MultipicController;
use Illuminate\Support\Facades\Route;
use App\Models\User;
use Illuminate\Support\Facades\DB;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/admin', function () {
    return view('backend.home');
});

Route::get('/', function () {
    return view('welcome');
});

Route::get('/hello', function(){
    echo "Hello!";
})->middleware('checkage');
//Route::get('/hello', [HelloController::class, 'index'])->middleware('checkage');

Route::get('/hello1', [HelloController::class, 'index']);

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    //$users = User::all();
    $users = DB::table('users')
            ->select('name','email','created_at')->get();
    return view('dashboard', compact('users'));
})->name('dashboard');


//Category
Route::resource('category', CategoryController::class)->middleware('auth:sanctum');
//Soft Delete
Route::get('softdelete/category/{id}', [CategoryController::class, 'SoftDelete']);

Route::get('/category/restore/{id}', [CategoryController::class, 'Restore']);
Route::get('/pdelete/category/{id}', [CategoryController::class, 'Pdelete']);


//Brand
Route::resource('brand', BrandController::class);
Route::get('/brand/delete/{id}', [BrandController::class, 'Delete']);


// Multi Image
Route::resource('multipic', MultipicController::class);


//
